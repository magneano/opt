from dolfin import *
from dolfin_adjoint import *
import mshr
adj_reset()

N = 64
mesh0 = UnitSquareMesh(N, N)
c = mshr.Circle(Point(0.5, 0.5), .2)
mesh1 = mshr.generate_mesh(c, N)

mesh = MultiMesh()
mesh.add(mesh0)
mesh.add(mesh1)
mesh.build()

V = MultiMeshFunctionSpace(mesh, "CG", 1)
W = MultiMeshFunctionSpace(mesh, "CG", 1)

n = FacetNormal(mesh)
h = Constant(2) * Circumradius(mesh)
u, v = TrialFunction(V), TestFunction(V)
w = MultiMeshFunction(W)

alpha = beta = Constant(4)
a = (inner(grad(u), grad(v)) * dX
     - dot(avg(grad(u)), jump(v, n))*dI 
     - dot(avg(grad(v)), jump(u, n))*dI 
     + (alpha/avg(h)) *inner(jump(u), jump(v)) *dI 
     + beta*inner(jump(grad(u)), jump(grad(v)))*dO)

L = w * v * dX

boundary = CompiledSubDomain("on_boundary")
fd = FacetFunction("size_t", mesh0)
boundary.mark(fd, 1)
bc = MultiMeshDirichletBC(V, Constant(0.), fd, 1 , 0)

uh = MultiMeshFunction(V)
# solve
A = assemble_multimesh(a)
b = assemble_multimesh(L)
bc.apply(A, b)

solve(A, uh.vector(), b)



x = SpatialCoordinate(mesh)
d = sin(pi*x[0])*sin(pi*x[1])
#d = MultiMeshFunction(W)

alpha = 1e-1

J_full = Functional((Constant(0.5) * inner(uh - d, uh - d)) * dX
                    +Constant(0.5) * Constant(alpha) * w**2 * dX)
control = Control(w)
J = ReducedFunctional(J_full, control)

# Optimization
from opt import *
def F(x):
    return J(x)

def D(x):
    J(x)
    d = J.derivative()[0]
    return d

def H(x, y):
    J(x)
    h = J.hessian(y)
    return h


x = Vector(w, inner(u,v) * dX)

f = Functional(F, D, H)

x_opt = minimize(x, f, method = "LBFGS", atol = 1e-10, rtol = 1e-15)
f_opt = x_opt.data


F =  2*pi**2/(1 + alpha * 4*pi**4) * d
error = assemble_multimesh((F-f_opt)**2 * dX)**.5

print "\nL^2 error in computed optimal control is ", error 
