from dolfin import *
from dolfin_adjoint import *
adj_reset()

N = 32
mesh = UnitSquareMesh(N, N)
V = FunctionSpace(mesh, "CG", 1)
W = FunctionSpace(mesh, "CG", 1)
u, v = TrialFunction(V), TestFunction(V)
w = interpolate(Expression("0",degree = 1), W, name='Control')

a = inner(grad(u), grad(v)) * dx
L = w * v * dx
        
bc = DirichletBC(V, 0., "on_boundary")
        
uh = Function(V)
solve(a == L, uh, bc)

d = Expression("sin(pi*x[0])*sin(pi*x[1])", degree = 3)
alpha = 1e-1

J_full = Functional((0.5 * inner(uh - d, uh - d)) * dx 
                    +0.5 * Constant(alpha) * w**2 * dx)
control = Control(w)
J = ReducedFunctional(J_full, control)

# Optimization
from opt import *
def F(x):
    return J(x)

def D(x):
    J(x)
    d = J.derivative()[0]
    return d

def H(x, y):
    J(x)
    h = J.hessian(y)
    return h


x = Vector(w, "L2")
f = Functional(F, D, H)
x_opt = minimize(x, f, method = "Newton", atol = 1e-10, rtol = 1e-15)
f_opt = x_opt.data


# compute error 
#F = Expression("c*d", c = 2*pi**2/(alpha +4*pi**4), d = d, degree = 3)
F = Expression("c*d", c = 2*pi**2/(1 + alpha * 4*pi**4), d = d, degree = 3)
error = errornorm(F, f_opt)

print "\nL^2 error in computed optimal control is ", error 
