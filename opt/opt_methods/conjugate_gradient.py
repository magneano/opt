from descent_method import DescentMethod
from opt.base import Process

class ConjugateGradient(DescentMethod):
    def _generate_cg_methods_dict(cls):
        _dict = {}
        for attr in dir(cls):
            if not "compute_beta_" in attr: pass
            _dict[attr.strip("compute_beta_")] = attr
        return _dict

    def list_cg_methods(cls):
        return cls._generate_cg_methods_dict().keys()

    @property 
    def minimum_history_length(cls):
        return 1

    def __init__(self, **kwargs):
        self.cg_method = kwargs.pop("cg_method", "PR")

    def set_cg_method(self, cg_method):
        # check that the method is known
        if not cg_method in self.list_cg_methods():
            err_msg = "Not a recognized conjugate gradient method."
            raise ValueError(err_msg)

        self._prev_P = None
        self._cg_method = cg_method

        # choose the right compute_beta for the method
        setattr(self, "compute_beta", getattr(self, self._generate_cg_methods_dict()[cg_method]))

    def get_cg_method(self):
        return self._cg_method

    def compute_direction(self, process):
        if not isinstance(process, Process):
            err_msg = "Not a valid process"
            raise AttributeError(err_msg)

        if process.prev == None or self.old_direction == None:
            # then do a steepest descent step
            P = (- process.G)

        else:
            D = process.D
            G = process.G

            D_prev = process.prev.D
            G_prev = process.prev.G

            P_prev = self.old_direction

            beta = self.compute_beta(D, G, D_prev, G_prev, P_prev)            
            P = - G + beta * P_prev

        # store direction, then return
        self.old_direction = P
        return P

    def get_old_direction(self):
        return self._prev_P
        
    def set_old_direction(self, P):
        self._prev_P = P

    def reset(self):
        self.set_old_direction(None)

    old_direction = property(fget = get_old_direction, fset = set_old_direction, fdel = reset)

    cg_method = property(fget = get_cg_method, fset = set_cg_method)

    def compute_beta_FR(self, D, G, D_prev, G_prev, P_prev):
        return D.apply(G) / D_prev.apply(G_prev)

    def compute_beta_PR(self, D, G, D_prev, G_prev, P_prev):
        return D.apply(G-G_prev) / D_prev.apply(G_prev)

        
