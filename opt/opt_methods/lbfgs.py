from descent_method import DescentMethod, SteepestDescent
from opt.base import Process

class LBFGS(DescentMethod):
    def minimum_history_length(cls):
        return 1

    def __init__(self, **kwargs):
        self.memory_limit = kwargs.pop("memory_limit", 10)

    def set_memory_limit(self, new_limit):
        if not (isinstance(new_limit, int) and new_limit > 0) or new_limit == None:
            err_msg = "Memory limit must be an integer or None."
            raise TypeError(err_msg)
        self._memory_limit = new_limit
    
    def get_memory_limit(self):
        return self._memory_limit

    def bfgs_operator(self, process):
        # defines the bfgs mapping of dualvectors to vectors
        def _Hk(z, k):
            if -k == min(self.memory_limit, len(process.history)):
                y = process[0].D - process[-1].D
                s = process[0].x - process[-1].x
                t = y.apply(s) / y.apply(y.vector())

                return t * z.vector()

            else:
                y = process[k].D - process[k-1].D
                s = process[k].x - process[k-1].x
                r = y.apply(s)**-1

                _z = _Hk(z - r * y * z.apply(s), k-1)
                _z = _z - r * s * y.apply(_z)
                return _z + r * s * z.apply(s)

        def _H(z): return _Hk(z, 0)

        return _H
            

    def compute_direction(self, process):
        if len(process.history) == 0:
            # then do steepest descent
            return SteepestDescent().compute_direction(process, normalize = True)

        else:
            return - self.bfgs_operator(process)(process.D)
        
    memory_limit = property(fget = get_memory_limit, fset = set_memory_limit)

    
