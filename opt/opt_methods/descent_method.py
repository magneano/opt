from opt.base import Process

class DescentMethod(object):
    """
    Base class for gradient descent methods. 
    Computes descent directions, using
    gradient information.
    """
    def __init__(self, **kwargs):
        pass

    def compute_descent_direction(self, proc):
        raise NotImplementedError

    def post_linesearch_update(self, proc):
        raise NotImplementedError

    def reset(self):
        # Some methods may need to purge bad data if it fails to find a search direction
        pass

class SteepestDescent(DescentMethod):
    def compute_direction(self, process, normalize = False):
        if not isinstance(process, Process):
            err_msg = "Not a valid process"
            raise AttributeError(err_msg)

        d = - process.G

        if normalize:
            return (- process.G) / process.D.apply(process.G)**.5

        else:
            return (- process.G)
