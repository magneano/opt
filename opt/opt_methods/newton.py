from descent_method import DescentMethod, SteepestDescent
from opt.base import Process

class Newton(DescentMethod):
    def minimum_history_length(cls):
        return 0

    def __init__(self, **kwargs):
        self.rtol = kwargs.pop("cg_rtol", 1e-5)
        self.atol = kwargs.pop("cg_atol", 1e-12)
        self.itol = kwargs.pop("cg_max_it", 100)
        self.converged = False

    def get_tolerances(self):
        return self._rtol, self._atol, self._itol
        
    def set_tolerances(self, atol = None, rtol = None, itol = None):
        if atol != None: self._atol = atol
        if rtol != None: self._rtol = rtol
        if itol != None: self._itol = itol

    rtol = property(fget = lambda self: self.get_tolerances()[0],
                    fset = lambda self, tol: self.set_tolerances(rtol=tol))

    atol = property(fget = lambda self: self.get_tolerances()[1],
                    fset = lambda self, tol: self.set_tolerances(atol=tol))

    itol = property(fget = lambda self: self.get_tolerances()[2],
                    fset = lambda self, tol: self.set_tolerances(itol=tol))

    def compute_direction(self, process):
        x = process.x
        D = process.D
        self.converged = False

        # newton solve: H(x, d) = - D(x) to given tolerance
        # using conjugate gradient iterations

        r = - D             # residual
        p = Rr = r.vector() # work vectors

        rnorm = r.apply(p)
        rtest = rnorm * self.rtol
        def converged():
            if rnorm < min(rtest, self.atol):
                return True
            return False

        def H(p): 
            return process.functional.evaluate_hessian(x, p)

        i = 0
        d = None
        while not converged() and i < self.itol:
            Hp = H(p)
            c = Hp.apply(p)
            if c < 0: 
                err_msg = "Newton breakdown: curvature is negative ({})".format(c)
                raise RuntimeError(err_msg)
            a = rnorm / c

            if i == 0:
                d = a * p
            else:
                d += a * p
            r -= a * Hp

            Rr = r.copy().vector()

            rnorm, rnorm0 = r.apply(Rr), rnorm
            b = rnorm / rnorm0
            p *= b
            p += Rr

            i += 1

        if converged(): self.converged = True
        
        return d
        
    
