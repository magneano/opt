from opt.base.functional import Functional
from opt.vector.numpy_vector import NumpyVector, NumpyDualVector

class NumpyFunctional(Functional):
    def __init__(self, evaluate = None, derivative = None):
        """ Shorhand for defining Functionals using numpy vectors """
        f = lambda x: evaluate(x.data) if evaluate else None
        D = lambda x: NumpyDualVector(derivative(x.data)) if evaluate else None
        Functional.__init__(self, f, D)
