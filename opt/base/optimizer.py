from opt.base.vector import Vector, DualVector
from opt.base.functional import Functional

class State(object):
    """
    Base class holding data for the optmization process:
    
      functional: the functional to be minimized
    
      x: Point of evaluation
      F: Functional value at x
      D: Derivative at x
      G: Gradient at x

    """
    def __init__(self, x = None, F = None, D = None, G = None, 
                 functional = None):
        self.functional = functional
        self.update(x = x, F = F, D = D, G = G)

    def get_functional(self):
        return self.__functional
        
    def get_x(self):
        return self.__x

    def get_F(self):
        if self.__F != None: return self.__F
        else:
            if self.functional != None:
                return self.functional(self.x)
            else:
                err_msg = "Cannot evaluate functional: missing functional"
                raise AttributeError(err_msg)

    def get_D(self):
        if self.__D != None: return self.__D
        else:
            if self.functional != None:
                return self.functional.derivative(self.x)
            else:
                err_msg = "Cannot evaluate functional derivative: missing functional"
                raise AttributeError(err_msg)
                

    def get_G(self):
        if self.__G != None: return self.__G
        else: 
            return self.D.vector()

    def set_functional(self, functional):
        if functional != None and not isinstance(functional, Functional):
            err_msg = "Not a Functional instance: {}".format(type(functional))
            raise TypeError(err_msg)
        self.__functional = functional
        self.__F = None
        self.__D = None
        self.__G = None
            
    def set_x(self, x):
        if x != None and not isinstance(x, Vector):
            err_msg = "x (current point) must be Vector-type."
            raise TypeError(err_msg)
        self.__x = x

    def set_F(self, F):
        if F != None and not isinstance(F, float):
            err_msg = "F (functional value at x) must be flot-type."
            raise TypeError(err_msg)
        self.__F = F
            
    def set_D(self, D):
        if D != None and not isinstance(D, DualVector):
            err_msg = "D (current derivative) must be DualVector-type."
            raise TypeError(err_msg)
        self.__D = D

    def set_G(self, G):
        if G != None and not isinstance(G, Vector):
            err_msg = "G (current gradient) must be Vector-type."
            raise TypeError(err_msg)
        self.__G = G

    def update(self, x=None, F=None, D=None, G=None):
        self.x = x
        self.F = F
        self.D = D
        self.G = G

    def copy(self, deepcopy = False):
        if deepcopy:
            x = None if self.x == None else self.x.copy()
            D = None if self.D == None else self.D.copy()
            G = None if self.G == None else self.G.copy()
        else:
            x = self.x
            D = self.D
            G = self.G
            
        return State(x = x, F = self.F, D = D, G = G, functional = self.functional)
    
    functional = property(fget = get_functional, fset = set_functional)
    F = property(fget = get_F, fset = set_F)
    x = property(fget = get_x, fset = set_x)
    D = property(fget = get_D, fset = set_D)
    G = property(fget = get_G, fset = set_G)


class Process(State):
    """ Process is a State object with access to previous states """
    def __init__(self, x, functional = None, history_length = 1):
        State.__init__(self, x = x, functional = functional)
        self.__history = []
        self.history_length = history_length
        
    @property
    def history(self): return list(self.__history)
    
    def set_history_length(self, n):
        """
        Set maximum number of previous states remembered.
        negative number for no limit.
        """
        n = int(n)
        if n < 0 : n = -1
        self.__history_length = n
        
    def get_history_length(self):
        return self.__history_length
        
    history_length = property(fget = get_history_length,
                              fset = set_history_length)
    
    def forget_last(self):
        self.__history.pop(0)
    
    def move(self, x, F = None, D = None, G = None):
        if len(self.history) == self.history_length > 0:
            self.forget_last()
        self.__history.append(self.copy())
        self.update(x = x, F = F, D = D, G = G)
    
    
    def backtrack(self):
        old = self.__history.pop(-1)
        self.update(x = old.x, F = old.F, D= old.D, G = old.G)
        
    @property
    def prev(self):
        if len(self.history) > 0:
            return self.history[-1]
        else:
            return None

    def __getitem__(self, k):
        if not (isinstance(k, int) and k <= 0):
            err_msg = "Indices must be non-positive integers."
            raise IndexError(err_msg)
        if k == 0:
            return self.copy()
        if not -k <= len(self.history):
            err_msg = "Only have {} previous state(s) stored."
            raise IndexError(err_msg.format(len(self.history)))
        return self.history[k]

class Optimizer(Process):
    """Base class defining a generic optmization method.
    This base class manages the following base objects:

      * a Process instance, from which the class inherits. Used to
        store information on the current state, e.g., points,
        derivatives, and gradients

      * a OptMethod instance, that can use the data stored in the
        process to compute a descent direction

      * a LineSearch instance that can search for admissable points in
        a computed descent direction.

      * a fallback method in case the optimization algortithm fails 
        to find a descent direction.

    """
    def __init__(self, x_init, functional):
        Process.__init__(self, x_init, functional)
        self.__opt_method = None
        self.__linesearch = None
        self.__fallback = None


    #------------------------------------------------------
    # Attribute access
    def get_opt_method(self):
        return self.__opt_method

    def set_opt_method(self, opt_method):
        from opt.opt_methods.descent_method import DescentMethod
        if not isinstance(opt_method, DescentMethod):
            err_msg = "Not a valid descent method: {}".format(opt_method)
            raise TypeError(err_msg)
        
        self.__opt_method = opt_method
        
    def get_linesearch(self):
        return self.__linesearch

    def set_linesearch(self, ls):
        from opt.linesearch import LineSearch
        if not isinstance(ls, LineSearch):
            err_msg = "Not a LineSearch: {}".format(type(ls))
        self.__linesearch = ls

    def get_fallback_method(self):
        return self.__fallback

    def set_fallback_method(self, fallback):
        from opt.opt_methods.descent_method import DescentMethod
        if not isinstance(fallback, DescentMethod):
            err_msg = "Not a valid descent method: {}".format(opt_method)
            raise TypeError(err_msg)

        self.__fallback = fallback

    opt_method = property(fget = get_opt_method, fset = set_opt_method)
    fallback_method = property(fget = get_fallback_method, fset = set_fallback_method)
    linesearch = property(fget = get_linesearch, fset = set_linesearch)
    


    #------------------------------------------------------
    # Basic algorithmic functions
    def get_descent_direction(self):
        if self.opt_method == None:
            err_msg = "Set descent method first."
            raise AttributeError(err_msg)
        direction = self.opt_method.compute_direction(self)
        if not self.D.apply(direction) < 0:
            err_msg = "Direction is not a descent direction"
            raise RuntimeError(err_msg)
        return direction

    def get_fallback_direction(self):
        if self.fallback_method == None:
            err_msg = "Set descent method first"
            raise AttributeError(err_msg)
        direction = self.fallback_method.compute_direction(self)
        if not self.D.apply(direction) < 0:
            err_msg = "Direction is not a descent direction"
            raise RuntimeError(err_msg)
        return direction

    def _is_descent_direction(self, direction):
        df = self.D.apply(direction)
        return df < 0

    def perform_linesearch(self, direction):
        try:
            ret = self.linesearch.search(self, direction)
        except Warning as w:
            err_msg = "Linesearch warning: " + w.message
            try:
                self.linesearch.ignore_warnings = True
                ret = self.linesearch.search(self, direction)
                self.linesearch.ignore_warnings = False
            except Exception as e:
                self.linesearch.ignore_warnings = False
                raise RuntimeError(err_msg + w.message + " followed by " +e.message)

        except AssertionError as e:
            # should not get here
            err_msg = "Linesearch breakdown"
            raise RuntimeError(err_msg)

        return ret

    def next(self):
        # determine direction
        try:
            direction = self.get_descent_direction()
        except RuntimeError as e1:
            if self.fallback_method != None:
                try:
                    print "failed to obtain search direction (fallback used): " + e1.message
                    direction = self.get_fallback_direction()
                except RuntimeError as e2:
                    err_msg = "Failed to obtain search direction: " + e1.message \
                            + " followed by " + e2.message
                    raise RuntimeError(err_msg)
            else:
                err_msg = "Failed to obtain direction (no fallback): " + e1.message
                raise RuntimeError(err_msg)
                
        
        # determine step
        a, x, F, D = self.perform_linesearch(direction)

        # update process
        self.move(x = x, F = F, D = D, G = None)
        

class OptimizationSolver(Optimizer):
    def __init__(self, x, f, **kwargs):
        Optimizer.__init__(self, x, f)

        # determine methods
        method = kwargs.pop("method", "LBFGS")
        linesearch = kwargs.pop("linesearch", "StrongWolfe")
        fallback_method = kwargs.pop("fallback_method", "SteepestDescent")
        import opt.linesearch
        import opt.opt_methods
        self.opt_method = getattr(opt.opt_methods, method)(**kwargs)
        self.linesearch = getattr(opt.linesearch, linesearch)(**kwargs)
        self.fallback_method = getattr(opt.opt_methods, fallback_method)(**kwargs)

        # determine tolerances
        atol = kwargs.pop("atol", 1e-9)
        rtol = kwargs.pop("rtol", 1e-6)
        max_it = kwargs.pop("max_it", 100)

        self.atol = atol
        self.rtol = rtol
        self.max_it = max_it

    def set_tolerances(self, atol = None, rtol = None, max_it = None):
        if atol != None:
            assert atol >= 0
            self.atol = float(atol)

        if rtol != None:
            assert rtol >= 0
            self.rtol = float(rtol)

        if max_it != None:
            assert type(max_it) == int and max_it > 0
            self.max_it = max_it

    def solve(self):
        n =  0
        N = self.max_it

        f_vals = self.f_vals = []
        n_vals = self.n_vals = []

        out = "Iteration {0:3d}:\n     f(x) = {1:1.6e}  ({2:1.6e})\n  |Df(x)| = {3:1.6e}  ({4:1.6e})\n"

        while n <= N:
            # update 
            f_vals.append(self.F)
            n_vals.append(self.D.apply(self.G)**.5)

            print out.format(n, 
                             f_vals[-1], f_vals[-1]/f_vals[0], 
                             n_vals[-1], n_vals[-1]/n_vals[0] )


            # test for gradient norm
            if n_vals[-1] < self.atol or n_vals[-1] < self.rtol * n_vals[0]:
                print "Iteration {0:3d}: Gradient norm converged".format(n) 
                break

            # test for max iter
            if n == N:
                print "Maximum of {} iterations reached".format(n)
                break

            # do another iteration
            try:
                self.next()
            except RuntimeError as e:
                print "Iteration {0:3d}: Breakdown (failed to obtain descent step: {1:s})".format(n, e.message)
                break
            n += 1


def minimize(x, f, **kwargs):
    solver = OptimizationSolver(x, f, **kwargs)
    solver.solve()
    return solver.x
