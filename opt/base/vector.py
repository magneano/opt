class InnerProduct(object):
    def __init__(self, interface):
        self.__interface = interface
        self.__name__ = "unknown"

    def assignment_update(self, vec):
        # some inner products may need information from a vector.
        pass

    def __call__(self, x, y):
        # implement in subclass
        raise NotImplementedError

    def dual_to_vector(self, x):
        # implement in subclass
        raise NotImplementedError

    def vector_to_dual(self, x):
        # implement in subclass
        raise NotImplementedError

    def __repr__(self):
        return "{}({})".format(self.__name__, str(self.interface))

    def __str__(self):
        return "{}({})".format(self.__name__, str(self.interface))

    def rename(self, name):
        assert type(name) == str
        self.__name__ = name

    @property
    def interface(self): return self.__interface

    def __eq__(self, other):
        raise NotImplementedError


class EuclidianInnerProduct(InnerProduct):
    def __init__(self, interface):
        InnerProduct.__init__(self, interface)
        self.rename("l2")

    def __call__(self, x, y):
        return self.interface.dot(x.data, y.data)

    def dual_to_vector(self, x):
        return Vector(x.copy(), x.ip) 

    def vector_to_dual(self, x):
        return DualVector(x.copy(), x.ip)

    def __eq__(self, other):
        return (self.__class__ == other.__class__ and 
                self.interface == other.interface)


class GenericInnerProduct(InnerProduct):
    def __init__(self, mat, inv, interface, name = None):
        InnerProduct.__init__(self, interface)
        self.__mat = mat
        self.__inv = inv
        if not name: name = "user"
        self.rename(name)

    def __call__(self, x, y):
        assert self.interface == x.interface == y.interface
        z = self.interface.matvec(self.mat, x.data)
        return self.interface.dot(z, y.data)

    def dual_to_vector(self, x):
        assert self.interface == x.interface
        assert x.__class__ == DualVector
        return Vector(VectorBase(self.interface.matvec(self.inv, x.data), self.interface), x.ip)

    def vector_to_dual(self, x):
        assert self.interface == x.interface
        assert x.__class__ == Vector
        return DualVector(VectorBase(self.interface.matvec(self.mat, x.data), self.interface), x.ip)

    @property
    def inv(self): return self.__inv

    @property
    def mat(self): return self.__mat

    def __eq__(self, other):
        if not (self.__class__ == other.__class__ and 
                self.interface == other.interface):
            return False
        # otherwise check that the operators are the same
        return (self.interface.mat_eq(self.mat, other.mat) and
                self.interface.mat_eq(self.inv, other.inv))



class VectorBase(object):
    def __init__(self, data, interface = None):
        if isinstance(data, VectorBase):
            assert interface == None
            v = data
            self.__data = v.data
            self.__interface = v.interface

        else:
            from opt.interfaces import InterfaceManager
            if interface == None:
                self.__interface = InterfaceManager().get_interface(data)
                self.__data = self.interface.process_data(data)
            else:
                #assert InterfaceManager().is_interface(interface)
                self.__interface = interface
                self.__data = data

    def __repr__(self):
        return "Vector({}; {}) of size {} at {}".format(repr(self.data), str(self.interface),
                                                      len(self), hex(hash(self)))

    def __str__(self):
        return "Vector({})".format(str(self.data))

    def __len__(self):
        return self.interface.vec_size(self.data)

    def __add__(self, other):
        assert_compatible(self, other)
        ret = self.copy(deepcopy = False)
        ret._VectorBase__data = self.interface.add(self.data, other.data)
        return ret

    def __iadd__(self, other):
        assert_compatible(self, other)
        self.interface.iadd(self.data, other.data)
        return self

    def __sub__(self, other):
        assert_compatible(self, other)
        if hasattr(self.interface, "sub"):
            ret = self.copy(deepcopy = False)
            ret._VectorBase__data = self.interface.add(self.data, other.data)
            return ret
        else:
            return self  + (- other)

    def __isub__(self, other):
        assert_compatible(self, other)
        if hasattr(self.interface, "isub"):
            self.interface.isub(self.data, other.data)
            return self
        else:
            self += -other
            return self

    def __mul__(self, other):
        assert_scalar(other)
        ret = self.copy(deepcopy = False)
        ret._VectorBase__data = self.interface.mul(self.data, other)
        return ret

    def __rmul__(self, other):
        return self.__mul__(other)
        
    def __imul__(self, other):
        assert_scalar(other)
        self.interface.imul(self.data, other)
        return self

    def __div__(self, other):
        assert_scalar(other)
        if hasattr(self.interface, "div"):
            ret = self.copy(deepcopy = False)
            ret._VectorBase__data = self.interface.div(self.data, other)
            return ret
        else:
            return self * other**-1
        
    def __idiv__(self, other):
        assert_scalar(other)
        if hasattr(self.interface, "idiv"):
            self.interface.idiv(self.data, other)
            return self
        else:
            self *= other**-1
        
    def __neg__(self):
        if hasattr(self.interface, "neg"):
            ret = self.copy(deepcopy = False)
            ret._VectorBase__data = self.interface.neg(self.data)
            return ret
        else:
            return self * -1
        
    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        if not self.interface == other.interface:
            return False
        if not len(self) == len(other):
            return False
        return self.interface.eq(self.data, other.data)

    def _dot(self, other):
        assert self.interface == other.interface
        assert len(self) == len(other)
        return self.interface.dot(self.data, other.data)
    
    def copy(self, deepcopy = True):
        if deepcopy:
            return VectorBase(self.interface.copy(self.data), self.interface)
        else:
            return VectorBase(self.data, self.interface)

    @property
    def data(self): return self.__data

    @property
    def interface(self): return self.__interface


def assert_compatible(x, y):
    assert x.__class__ == y.__class__
    assert x.interface == y.interface
    assert len(x) == len(y)


def assert_scalar(x):
    from numpy import issctype
    assert issctype(type(x))


class IPVector(VectorBase):
    # class exists only to avoid code duplication in the two subclasses
    def __init__(self, data, inner_product = "default"):
        VectorBase.__init__(self, data)
        if type(inner_product) == str and inner_product == "default":
            self.ip = self.interface.get_default_inner_product()
        elif isinstance(inner_product, (EuclidianInnerProduct, GenericInnerProduct)):
            assert inner_product.interface == self.interface
            self.ip = inner_product
        else:
            self.ip = self.interface.process_ip(inner_product)
        self.ip.assignment_update(self)

    def copy(self, deepcopy = True):
        if deepcopy:
            return self.__class__(VectorBase.copy(self), self.ip)
        else:
            return self.__class__(self, self.ip)

    def __str__(self):
        return "{}({}; {})".format(self.__class__.__name__, str(self.data), str(self.ip))

    def __repr__(self):
        return "{}({}; {}) of size {} at {}".format(self.__class__.__name__, repr(self.data), repr(self.ip), len(self), hex(hash(self)))


class Vector(IPVector):
    def dual(self):
        return DualVector(self.ip.vector_to_dual(self), self.ip)

    def inner(self, other):
        if not (other.__class__ == Vector and self.ip == other.ip):
            err_msg = "inner products need compatible Vector types"
            raise TypeError(err_msg)

        return self.ip(self, other)

    def norm(self):
        return self.inner(self)**0.5

class DualVector(IPVector):
    def vector(self):
        return Vector(self.ip.dual_to_vector(self), self.ip)

    def apply(self, other):
        return self._dot(other)
