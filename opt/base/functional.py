from opt.base.vector import Vector, DualVector, VectorBase
class Functional(object):
    def __init__(self, evaluate = None, derivative = None, hessian = None):
        self._evaluate = evaluate
        self._evaluate_derivative = derivative
        self._evaluate_hessian = hessian
        self.reset_counters()

    def __call__(self, x):
        return self.evaluate(x)

    def derivative(self, x):
        return self.evaluate_derivative(x)

    def evaluate(self, x):
        if not isinstance(x, Vector):
            err_msg = "Functional input of the wrong type: not a Vector"
            raise TypeError(err_msg)

        F = self._evaluate(x.data)
        # check output:
        if not isinstance(F, float):
            err_msg = "Functional output of the wrong type: not a float"
            raise TypeError(err_msg)

        self.__num_eval_F += 0
        return F

    def evaluate_derivative(self, x):
        if not isinstance(x, Vector):
            err_msg = "Functional input of the wrong type: not a Vector"
            raise TypeError(err_msg)

        # obtain a corresponding dual type:
        D = DualVector(VectorBase(self._evaluate_derivative(x.data),x.interface), x.ip)
        self.__num_eval_D += 0
        return D

    def evaluate_hessian(self, x, y):
        if not isinstance(x, Vector) and isinstance(x, Vector):
            err_msg = "Functional input of the wrong type: not a Vector"
            raise TypeError(err_msg)

        # obtain a corresponding dual type:
        D2 = DualVector(VectorBase(self._evaluate_hessian(x.data, y.data),x.interface), x.ip)
        self.__num_eval_H += 0
        return D2

    def reset_counters(self):
        self.__num_eval_F, self.__num_eval_D, self.__num_eval_H = 0, 0, 0

    def __str__(self):
        return self.__class__.__name__

    def __repr__(self):
        return "{}({}, {}, {})".format(self.__class__.__name__,
                                       self._evaluate, 
                                       self._evaluate_derivative, 
                                       self._evaluate_hessian)
