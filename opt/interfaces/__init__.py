#from numpy_vector import NumpyVector, NumpyDualVector, Vector, DualVector
#from numpy_functional import NumpyFunctional
#__all__ = ["NumpyVector", "NumpyDualVector", "NumpyFunctional"]

from numpy_interface import NumpyInterface
from dolfin_interface import DolfinInterface
__interfaces__ = (NumpyInterface, DolfinInterface)

class InterfaceManager(object):
    def get_interface(self, data):
        # try to identify suitable interface for the data
        for ic in __interfaces__:
            interface = ic()
            if interface.is_suitable(data):
                return interface

        err_msg = "Found no valid interface for data: {}".format(data)
        raise RuntimeError(err_msg)

    def is_interface(self, obj):
        return isinstance(obj, __interfaces__)

del NumpyInterface
