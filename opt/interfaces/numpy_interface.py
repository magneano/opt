# TODO: make interface a module instead of a class
import numpy
from interface import Interface
class NumpyInterface(Interface):
    def __str__(self):
        return "numpy"

    def is_suitable(self, data):
        # if input is not a numpy.array, check if it can be converted
        if isinstance(data, (list, tuple)):
            try:
                data = numpy.array(data)
            except:
                return False

        # make sure we have an array
        if not isinstance(data, numpy.ndarray):
            return False

        # make sure the array is vector-like with scalar entries
        if not numpy.issctype(data.dtype):
            return False
        if len(data.shape) > 2: 
            return False
        if len(data.shape) == 2:
            return(data.shape[0] == 1 or data.shape[1] == 1)
        return len(data.shape) == 1

    # --- Comparions tests --- 
    def eq(self, x, y):
        # check elementwise equality
        return all(x == y)

    def mat_eq(self, A, B):
        # check elementwise equality
        return A.data == B.data


    # --- data handling methods ---
    def process_data(self, data):
        # make sure the vectors have uniform structure
        # copying is needed if input is list or tuple or not float
        if isinstance(data, (list, tuple)):
            data = numpy.array(data, dtype = float)
        #if data.dtype != float: data = data.astype(float)
        data.shape = (len(data),)
        return data

    def process_ip(self, mat):
        if type(mat) == str and mat == "l2":
            from opt.base.vector import EuclidianInnerProduct
            return EuclidianInnerProduct(self)

        assert isinstance(mat, numpy.ndarray)
        assert numpy.issctype(mat.dtype)

        from opt.base.vector import GenericInnerProduct
        return GenericInnerProduct(mat, numpy.linalg.inv(mat), self)


   
