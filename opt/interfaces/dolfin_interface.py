import dolfin
import dolfin.cpp
from interface import Interface

class DolfinInterface(Interface):
    def __str__(self):
        return "dolfin"

    def is_suitable(self, data):
        return isinstance(data, (dolfin.Function, dolfin.MultiMeshFunction))

    def vec_size(self, data):
        return data.vector().local_size()

    def copy(self, data):
        #return dolfin.cpp.Function(data._function_space(), data.vector().copy())
        #return dolfin.Function(data.function_space(), data.vector().copy())
        return data.__class__(data.function_space(), data.vector().copy())

    # --- Linear algebra operations ---
    def add(self, x, y):
        #return dolfin.cpp.Function(x._function_space(), x.vector() + y.vector())
        #return dolfin.Function(x.function_space(), x.vector() + y.vector())
        assert x.__class__.__name__ == y.__class__.__name__
        return x.__class__(x.function_space(), x.vector() + y.vector())

    def iadd(self, x, y):
        x.vector().__iadd__(y.vector())

    def mul(self, x, a):
        #return dolfin.cpp.Function(x._function_space(), a * x.vector())
        #return dolfin.Function(x.function_space(), a * x.vector())
        return x.__class__(x.function_space(), a * x.vector())

    def imul(self, x, a):
        x.vector().__imul__(a)

    def dot(self, x, y):
        return x.vector().inner(y.vector())

    def matvec(self, mat, x):
        # assuming square matrix here
        if isinstance(mat, dolfin.GenericMatrix):
            #return dolfin.cpp.Function(x._function_space(), mat * x.vector())
            #return dolfin.Function(x.function_space(), mat * x.vector())
            return x.__class__(x.function_space(), mat * x.vector())

        elif isinstance(mat, dolfin.LinearSolver):
            #ret = dolfin.cpp.Function(x.function_space())
            #ret = dolfin.Function(x.function_space())
            ret = x.__class__(x.function_space())
            mat.solve(ret.vector(), x.vector())
            return ret

        else:
            err_msg = "Unhandled object {}".format(mat)
            raise TypeError(err_msg)

    # --- Comparions tests --- 
    def eq(self, x, y):
        # check elementwise equality
        return all(x.vector() == y.vector())

    # --- Input handling methods ---
    def process_data(self, data):
        # work with dolfin.cpp.Function class for significant speedup for small and moderate sized vectors
        #return dolfin.cpp.Function(data._function_space(), data.vector())
        return data #dolfin.cpp.Function(data.function_space(), data.vector())
        

    def process_ip(self, ip):
        if type(ip) == str:
            if ip == "l2":
                from opt.base.vector import EuclidianInnerProduct
                return EuclidianInnerProduct(self)

        return DolfinInnerProduct(interface = self, form = ip)
                

from opt.base.vector import GenericInnerProduct
class DolfinInnerProduct(GenericInnerProduct):
    __buffer__ = {} # FIXME: current implementation using form as key will miss changing mesh
    def __init__(self, form = None, interface = DolfinInterface(), name = None):
        GenericInnerProduct.__init__(self, None, None, interface)
        self.form = form
        self.function_space = None
        self._assembler = None
        if type(form) == str:
            self.__name__ = form

    def assignment_update(self, x):
        self.function_space = x.data.function_space()
        self._assemble()

    def __eq__(self, other):
        return (GenericInnerProduct.__eq__(self, other) and
                self.function_space == other.function_space)

    def _assemble(self):
        if type(self.form) == str:
            form_type = self.form
            self.form = self._init_form(form_type, self.function_space)

        from ufl import Form, adjoint

        # check that we have a form
        assert isinstance(self.form, Form)

        # check that form is bilinear
        assert len(self.form.arguments()) == 2 

        # check that the form is symmetric DOES NOT WORK
        #assert self.form.equals(adjoint(self.form))

        # check if form is buffered to avoid duplication of matrices
        if self.form.signature() in self.__buffer__:
            A, B = self.__buffer__[self.form.signature()]

        else:
            # determine assembler
            if not self._assembler:
                if _has_multimesh(self.form):
                    A = dolfin.assemble_multimesh(self.form)
                else:
                    A = dolfin.assemble(self.form)
            B = dolfin.LinearSolver("lu")
            B.set_operator(A)
            B.parameters["reuse_factorization"] = True
            self.__buffer__[self.form.signature()] = (A, B)

        self._GenericInnerProduct__mat = A
        self._GenericInnerProduct__inv = B


    def _init_form(self, form_type, V):
        if form_type == "L2":
            return dolfin.inner(dolfin.TrialFunction(V), dolfin.TestFunction(V)) * dolfin.dx()
        

def _has_multimesh(form):
    multimesh_integral_types = ["cutcell", "overlap", "interface"]

    for integral in form.integrals():
        if integral.integral_type() in multimesh_integral_types:
            return True

    return False
