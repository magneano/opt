class Interface(object):
    """This class provides a template for interfaces. In general, nearly
    all the methods have to be overloaded in subclasses. The
    implementation is this base class are suited for numpy data.
    
    """
    def is_suitable(self, data):
        """this function evaluates if the interface is suitable given a
        user-provided input.

        """
        raise NotImplementedError

    def __str__(self):
        """ Name of the backend (eg numpy, dolfin)."""
        raise NotImplementedError

    def __eq__(self, other):
        return self.__class__ == other.__class__

    def copy(self, x):
        """ returns a (proper) copy of the data """
        return x.copy()

    def vec_size(self, vec):
        """ returns the (local) size of the vector """
        return len(vec)

    # --- Linear algebra operations ---
    def add(self, x, y):
        return x + y

    def iadd(self, x, y):
        x += y

    def mul(self, x, a):
        return x * a

    def imul(self, x, a):
        x *= a

    # --- Additional operations used if provided ---
    """
    def sub(self, x, y):
        return x - y

    def isub(self, x, y):
        x -=y

    def div(self, x, a):
        return x / a

    def idiv(self, x, a):
        x /= a
    """


    def dot(self, x, y):
        """ standard dot product on R^n. """
        return x.dot(y)


    def matvec(self, mat, vec):
        """ matrix - vector product. """
        return mat.dot(vec)


    # --- Comparions tests --- 
    def eq(self, x, y):
        """Not (currently) a necessary function, but may be useful for user
        convenience, hence the nature of the comparison is
        unspecified.

        """
        return x == y
        
    def mat_eq(self, A, B):
        """This is (currently) only used for determining if inner products
        coincide. Can be overloaded to return True to avoid this test.

        """
        return A == B
        

    # --- Input handling methods ---
    def process_data(self, data):
        """The role of this function is to provide uniform handling of
        possible non-uniform user input. For example, make sure that
        numpy arrays have shape (n,) rather than (n,1) or (1,n), and
        make sure that list and tuple inputs are converted into numpy
        arrays.

        """
        raise NotImplementedError


    def process_ip(self, mat):
        """The role of this function is to provide uniform handling of
        possible non-uniform user input.

        """
        raise NotImplementedError


    def get_default_inner_product(self):
        """Overload in subclass to change default from euclidian."""
        from opt.base.vector import EuclidianInnerProduct
        return EuclidianInnerProduct(self)
