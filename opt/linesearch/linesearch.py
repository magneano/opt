class LineSearch(object):
    def search(self, proc):
        err_msg = "Must be implemented in subclass."
        raise NotImplementedError(err_msg)
