from opt.base import Functional, Process, Optimizer, Vector, DualVector

from opt.linesearch import StrongWolfe
from opt.opt_methods import SteepestDescent

import numpy

F = lambda x    : (1-x[0])**2 + 100*(x[1] - x[0]**2)**2
D = lambda x    : numpy.array([-2*(1-x[0]) + 100*2*(x[1]-x[0]**2)*(-2*x[0]),
                               100*2*(x[1] - x[0]**2)])
H = None

x = Vector(numpy.array([-3., -4.]))
y = Vector(numpy.array([1., 2.]))
z = Vector(numpy.array([3., 1.]))

f = Functional(F, D)



def test_init():
    p = Optimizer(x, f)
    p.opt_method = SteepestDescent()
    p.linearsearch = StrongWolfe()


def test_direction():
    p = Optimizer(x, f)
    p.opt_method = SteepestDescent()
    p.linesearch = StrongWolfe()
    d = p.get_descent_direction() 
    assert d == Vector([ 15608.,  2600.])


def test_update():
    p = Optimizer(x, f)
    p.opt_method = SteepestDescent()
    p.linesearch = StrongWolfe()
    d = p.get_descent_direction()
    a, _, _, _ = p.perform_linesearch(d)
    
    p.next()
    
    assert x + a * d == p.x
    

