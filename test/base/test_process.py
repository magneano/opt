from opt.base import Functional, Process
from opt.base.vector import Vector, DualVector
import numpy

F = lambda x    : x.dot(x)
D = lambda x    : 2*x
H = lambda x, y : 2*y

x = Vector(numpy.array([0., 0.]))
y = Vector(numpy.array([1., 2.]))
z = Vector(numpy.array([3., 1.]))

f = Functional(F, D)
def test_process_init():
    try:
        p = Process(y, functional = f)
    except:
        raise AssertionError
    assert p


def test_get_derivative():
    p = Process(y, functional = f)
    val = p.F
    assert val == 5

    val = p.D
    assert val.data[0] == 2
    assert val.data[1] == 4

def test_stepping():
    p = Process(x, functional = f, history_length=2)
    assert len(p.history) == 0
      
    p.move(y)
    assert len(p.history) == 1
 
    p.move(z)
    assert len(p.history) == 2

    p.move(x)
    assert len(p.history) == 2
    assert p.prev.x.data[0] == z.data[0] \
        and p.prev.x.data[1] == z.data[1]

    p.backtrack()
    assert len(p.history) == 1
    assert p.x.data[0] == z.data[0] and p.x.data[1] == z.data[1]
    assert p.prev.x.data[0] == y.data[0] and p.prev.x.data[1] == y.data[1]
    
    p.forget_last()
    assert len(p.history) == 0

def test_getitem():
    p = Process(x, functional = f, history_length=3)
    p.move(y)
    p.move(z)
    p.move(x)

    assert len(p.history) == 3

    try:
        s = p[1]
        raise AssertionError
    except IndexError:
        pass

    try:
        s = p[-4]
        raise AssertionError
    except IndexError:
        pass

    assert x == p.x == p[-3].x
    assert z == p.prev.x == p[-1].x
    assert y == p[-2].x

