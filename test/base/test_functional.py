from opt.base.functional import Functional
from opt.base.vector import Vector, DualVector
import numpy

F = lambda x    : x.dot(x)
D = lambda x    : 2*x
H = lambda x, y : 2*y

 
x = Vector(numpy.array([0., 0.]))
y = Vector(numpy.array([1., 2.]))
z = DualVector(numpy.array([0., 0.]))

def test_functional_init():
    f = Functional(F, D)
    f = Functional(F, D, H)

def test_functional_eval():
    f = Functional(F, D)
    assert F(x.data) == f(x) == 0
    assert F(y.data) == f(y) == 5

    try:
        # trying an illegal evaluation
        f_z = f(z)
        raise AssertionError
    except TypeError:
        pass

def test_functional_derivative():
    f = Functional(F, D)
    d = f.derivative(x)
    assert isinstance(d, DualVector)
    assert all(D(x.data) == d.data)

    try:
        # trying an illegal evaluation
        d = f.derivative(z)
        raise AssertionError
    except TypeError:
        pass


if __name__ == "__main__":
    test_functional_init()
    test_functional_eval()
    test_functional_derivative()
