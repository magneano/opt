import opt
import numpy
from opt.base.vector import Vector, DualVector

def test_types():
    x = numpy.arange(5)

    u = Vector(x)
    try:
        v = u.vector()
        raise AssertionError
    except AttributeError:
        pass

    y = numpy.ones(5)
    v = DualVector(y)
    try:
        w = v.dual_vector()
        raise AssertionError
    except AttributeError:
        pass

    try:
        r = u.inner(v)
        raise AssertionError
    except TypeError:
        pass

    # Check that riesz map is correct
    assert u.inner(v.vector()) == v.apply(u)

if __name__ == "__main__":
    test_types()
