from opt.base import Functional, Process, Optimizer
from opt.base.vector import Vector, DualVector
import numpy

F = lambda x    : x.dot(x)
D = lambda x    : 2*x
H = lambda x, y : 2*y


x = Vector(numpy.array([1., 2.]))
f = Functional(F, D)

from opt.opt_methods import SteepestDescent
from opt.linesearch import StrongWolfe

def test_wolfe():
    p = Optimizer(x, f)
    p.linesearch = StrongWolfe(ignore_warnings = True)
    p.opt_method = SteepestDescent()
    

    direction = p.get_descent_direction()
    step, y, Fy, Dy = p.perform_linesearch(direction)

    assert (Fy - 5e-8) < 1e-15



