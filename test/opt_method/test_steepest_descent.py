from opt.base import Functional, Process, Optimizer
from opt.base.vector import Vector, DualVector
import numpy

F = lambda x    : x.dot(x)
D = lambda x    : 2*x
H = lambda x, y : 2*y


x = Vector(numpy.array([1., 2.]))
f = Functional(F, D)

from opt.opt_methods import SteepestDescent
from opt.linesearch import StrongWolfe


def test_steepest_descent_numpy():
    method = SteepestDescent()
    d = method.compute_direction(Process(x, f))
    assert isinstance(d, Vector)
    assert d.data[0] == -2
    assert d.data[1] == -4
