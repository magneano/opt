import opt
import numpy
from opt.base.vector import VectorBase, Vector, DualVector

# initialization
def test_init():
    # init from numpy vector
    x = numpy.arange(5)
    v = VectorBase(x)
    assert v
    # also make sure data is not copied
    x[0] = 1
    assert v.data[0] == 1

    # init from list
    x = range(5)
    v = VectorBase(x)
    assert v

    # init from VectorBase instance
    w = VectorBase(v)
    assert w

    x = numpy.arange(5)
    v = Vector(x)
    assert v

    x = numpy.arange(5)
    v = DualVector(x)
    assert v

# copying and data access
def test_copy():
    x = numpy.arange(5)
    v = Vector(x)
    w = v.copy()
    assert w
    assert w == v
    assert w.data[3] == v.data[3] == 3
    v.data[2] = 0
    assert v.data[2] == 0
    assert w.data[2] == 2

# custom inner products and duality
def test_inner():
    x = numpy.arange(5)
    A = numpy.eye(5)
    v = Vector(x, A)
    assert v.dual()
    assert v.dual().ip == v.ip
    assert len(v) == 5
    assert max(abs(x - v.dual().data)) == 0
    ip = v.ip
    assert ip(v,v) == x.dot(x)

    w = Vector(x)
    assert w.dual()
    assert w.dual().ip == w.ip
    assert max(abs(x - w.dual().data)) == 0
    ip = v.ip
    assert ip(v,v) == x.dot(x)

    A = 2 * numpy.eye(5)
    u = Vector(x, A)
    v = u.dual()
    assert max(abs(2*x - v.data)) == 0
    w = v.vector()
    assert max(abs(x - w.data)) == 0
    assert u == w
    ip = u.ip
    assert ip(u,u) == (2 * x).dot(x)

# linear algebra
def test_linalg():
    x = numpy.arange(5, dtype = float)
    y = numpy.ones(5)

    u = Vector(x.copy())
    v = Vector(y.copy())

    # neg
    assert -u
    assert all((-u).data == - x)

    # add
    z = x + y
    w = u + v
    assert all(w.data == z)
    
    # iadd
    u += v
    assert all(u.data == z)

    # mul
    w = 3 * u
    all(w.data == 3*z)

    # imul
    u *= 3
    all(u.data == 3*z)

    # dot
    assert u._dot(u) == (3*z).dot(3*z)
    

def time_init(n = 16):
    import time
    t0 = time.clock()
    #x = numpy.zeros(n)
    x = numpy.arange(n, dtype = float)
    t1 = time.clock()

    T0 = t1-t0

    t0 = time.clock()
    X = Vector(x)
    t1 = time.clock()

    T1 = t1-t0

    print T1/T0

class NumpyVector(VectorBase):
    def copy(self):
        #return NumpyVector(self.data.copy(), self.interface)
        return NumpyVector(numpy.copy(self.data), self.interface)
        #return self.data.copy()
        #return numpy.copy(self.data)
        #return NumpyVector(self)
        #return NumpyVector(self.data.copy(), self.interface)

def _copy(x):
    return x.copy()
def copy(n = 16):
    x = numpy.arange(n, dtype = float)
    z = x.copy()
    X = NumpyVector(x)
    import time

    
    t0 = time.clock()
    y = x.copy()
    t1 = time.clock()

    T0 = t1-t0
    t0 = time.clock()
    Y = X.copy()
    t1 = time.clock()

    T1 = t1-t0


    
    print float(T1)/T0
    
       
if __name__ == "__main__":
    test_init()
    test_copy()
    test_inner()
    test_linalg()

    from timeit import timeit
    x = numpy.arange(10, dtype = float)
    X = NumpyVector(x)
    
    n = 10000
    y = x.copy()
    Y = X.copy()
    T0 = timeit("y = x.copy()", setup="from __main__ import x, X", number = n)
    T1 = timeit("y = _copy(x)", setup="from __main__ import x, X, _copy", number = n)
    T2 = timeit("y = X.copy()", setup="from __main__ import x, X", number = n)
    
    print T2 / T1
    #time_copy(100)
