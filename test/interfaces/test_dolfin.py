import opt
import dolfin
from dolfin import Function, FunctionSpace, UnitSquareMesh
import numpy
from numpy.random import randn
from opt.base.vector import VectorBase, Vector, DualVector

mesh = dolfin.UnitSquareMesh(8, 8)
F = dolfin.FunctionSpace(mesh, "CG", 1)

# initialization
def test_init():
    # init from dolfin function
    f = Function(F)
    u = VectorBase(f)
    assert isinstance(u.data, dolfin.cpp.Function)

    # init from VectorBase instance
    w = VectorBase(u)
    assert w
    
    w = Vector(w)
    assert w
    w = Vector(f)
    assert w
    
    w = DualVector(f)
    assert w

# copying and data access
def test_copy():
    f = Function(F)
    u = VectorBase(f)

    # check that u itself is not a copy
    f.vector()[:] = randn(f.vector().local_size())
    assert (u.data.vector()-f.vector()).norm("linf") == 0
    
    w = u.copy()
    # check that w is proper copy
    f.vector().zero()
    assert (f.vector() - u.data.vector()).norm("linf") == 0
    assert (w.data.vector() - u.data.vector()).norm("linf") != 0
    assert w != u



# custom inner products and duality
def test_inner():
    # euclidian inner product
    f = Function(F); f.vector()[:] = randn(f.vector().local_size())
    g = Function(F); g.vector()[:] = randn(g.vector().local_size())
    u = Vector(f, "l2")
    assert u
    assert u.ip
    v = u.dual()
    w = v.vector()
    assert v
    assert w
    assert w.ip == v.ip == u.ip
    w = Vector(g, "l2")
    assert u.ip == w.ip
    assert abs(u.inner(w) - f.vector().inner(g.vector())) < 1e-14
    
    # L2 inner product
    u = Vector(f, "L2")
    assert u
    assert u.ip
    v = u.dual()
    assert v
    w = v.vector()
    assert w
    assert (u.data.vector() - w.data.vector()).norm("linf") < 1e-14
    w = Vector(g, "L2")
    assert u.ip == w.ip
    assert abs(u.inner(w) - dolfin.assemble(f*g*dolfin.dx)) < 1e-14


# linear algebra
def test_linalg():
    f = Function(F); f.vector()[:] = randn(f.vector().local_size())
    g = Function(F); g.vector()[:] = randn(g.vector().local_size())
    h = f.copy(deepcopy = True)

    u = Vector(f, "L2")
    v = Vector(g, "L2")

    # add
    w = u + v
    assert all(w.data.vector() == f.vector() + g.vector())
    assert w.ip == u.ip
    # iadd
    u += v
    # because the constructor should not the data:
    assert all(u.data.vector() == f.vector()) 
    assert all(u.data.vector() == h.vector() + g.vector())

    # mul
    w = 2 * u
    assert all(w.data.vector() == 2 * f.vector())

    # imul
    u *= 2
    # because the constructor should not the data:
    assert all(u.data.vector() == f.vector()) 
    assert all(u.data.vector() == 2 *(h.vector() + g.vector()))

    # neg
    assert all((-u).data.vector() == -f.vector()) 

    # dot
    assert u._dot(v) == f.vector().inner(g.vector())



class DolfinVector(VectorBase):
    def copy(self):
        return DolfinVector(self.data.copy(deepcopy = True), self.interface)

    def __add__(self, other):
        V = self.data.function_space()
        z = self.data.vector() + other.data.vector()
        return DolfinVector(Function(V, z), self.interface)

def time_init(N = 16):
    from time import clock

    mesh = dolfin.UnitSquareMesh(N, N)
    V = FunctionSpace(mesh, "CG", 1)

    t0 = clock()
    u = Function(V)
    t1 = clock()
    T0 = t1-t0

    t0 = clock()
    U = Vector(Function(V))
    t1 = clock()
    T1 = t1-t0
    
    return T1/T0

def time_copy(N = 16):
    from time import clock

    mesh = dolfin.UnitSquareMesh(N, N)
    V = FunctionSpace(mesh, "CG", 1)
    u = Function(V)

    x = Vector(u)
    t0 = clock()
    v = u.copy(deepcopy = True)
    t1 = clock()
    T0 = t1-t0

    t0 = clock()
    y = x.copy()
    t1 = clock()
    T1 = t1-t0
    
    return T1/T0

def time_add(N = 16):
    from time import clock
    from dolfin import cpp
    mesh = dolfin.UnitSquareMesh(N, N)
    V = FunctionSpace(mesh, "CG", 1)

    u = Function(V); u.vector()[:] = randn(u.vector().local_size()) 
    v = Function(V); v.vector()[:] = randn(v.vector().local_size()) 

    x = Vector(u)
    y = Vector(v)

    t0 = clock()
    w = cpp.Function(V, u.vector()+v.vector())
    t1 = clock()
    T0 = t1-t0

    t0 = clock()
    z = x + y
    t1 = clock()
    T1 = t1-t0

    return T1/T0

    
if __name__ == "__main__":
    test_init()
    test_copy()
    test_inner()
    test_linalg()

    N = 10
    V = FunctionSpace(mesh, "CG", 1)
    print time_init(N)
    print time_copy(N)
    print time_add(N)
