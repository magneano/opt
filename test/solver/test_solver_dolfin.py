from dolfin import *
from dolfin_adjoint import *
adj_reset()

mesh = UnitSquareMesh(8, 8)
V = FunctionSpace(mesh, "CG", 1)
u, v = TrialFunction(V), TestFunction(V)
w = interpolate(Expression("x[0]+x[1]",degree = 1), V, name='Control')

a = inner(grad(u), grad(v)) * dx
L = w * v * dx
        
bc = DirichletBC(V, 0., "on_boundary")
        
uh = Function(V)
solve(a == L, uh, bc)

d = Constant(1.0)
alpha = Constant(1e-2)

J_full = Functional((0.5*inner(uh - d, uh - d))*dx + alpha/2*w**2*dx)
control = Control(w)
J = ReducedFunctional(J_full, control)

# Define the functional as needed for optimization
from opt.base import Functional, OptimizationSolver, Vector, DualVector, minimize
def F(x):
    return J(x)

def D(x):
    J(x)
    d = J.derivative()[0]
    return d

def H(x, y):
    J(x)
    h = J.hessian(y)
    return h

x = Vector(w, "L2")
f = Functional(F, D, H)

def test_solve_newton():
    # check that one newton iteration is enough
    solver = OptimizationSolver(x, f, method = "Newton",
                                cg_rtol = 1e-30, cg_atol = 1e-10,
                                rtol = 1e-8, atol = 1e-8)
    solver.solve()
    assert len(solver.f_vals) == 1 + 1

if __name__ == "__main__":
    solver = OptimizationSolver(x, f, method = "Newton",
                                max_it = 100,
                                ls_xtol = 1e-1,
                                cg_rtol = 1e-16,
                                cg_atol = 1e-16,
                                ls_ignore_warnings = True,
                                rtol = 1e-5, atol = 1e-8)
    solver.solve()


    
