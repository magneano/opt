from opt.base import Functional, OptimizationSolver, Vector, minimize

from opt.linesearch import StrongWolfe
from opt.opt_methods import SteepestDescent

import numpy

F = lambda x    : (1-x[0])**2 + 100*(x[1] - x[0]**2)**2

D = lambda x    : numpy.array([-2*(1-x[0]) + 100*2*(x[1]-x[0]**2)*(-2*x[0]),
                               100*2*(x[1] - x[0]**2)])

H = lambda x, d : numpy.array([[ 2. - 400. * x[1] + 1200.*x[0]**2 , -400. * x[0]],
                               [-400. * x[0], 200.]]).dot(d)

x = Vector(numpy.array([-3., -4.]))
y = Vector(numpy.array([1., 1.]))   # exact minimum
f = Functional(F, D, H)

def test_init():
    solver = OptimizationSolver(x, f)

def test_solve():
    solver = OptimizationSolver(x, f, method = "Newton",
                                rtol = 1e-15, atol = 1e-12)
    solver.solve()
    assert (solver.x - y).norm() < 1e-10

def test_minimize_lbfgs():
    z = minimize(x, f, method = "LBFGS",
                 rtol = 1e-15, atol = 1e-12)
    assert (z - y).norm() < 1e-10

def test_minimize_newton():
    z = minimize(x, f, method = "Newton",
                 rtol = 1e-15, atol = 1e-12)
    assert (z - y).norm() < 1e-10

def test_minimize_cg():
    z = minimize(x, f, method = "ConjugateGradient",
                 rtol = 1e-15, atol = 1e-12)
    assert (z - y).norm() < 1e-10

if __name__ == "__main__":
    x = minimize(x, f, method = "Newton", 
                 rtol = 1e-10, atol = 1e-10,
                 cg_method = "PR",
                 mem_lim = 10,
                 cg_rtol = 1e-1, cg_atol = 1e-1)
    print "x = ", x.data

